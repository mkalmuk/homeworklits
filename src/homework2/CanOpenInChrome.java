package homework2;

public interface CanOpenInChrome {
	
	public String getHTMLInChrome (String htmlContent);

}
