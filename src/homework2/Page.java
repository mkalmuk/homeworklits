package homework2;

abstract class Page {
	
	private String title;  
	private String url;
	private String htmlContent;
	
	Page (String title,  String url, String htmlContent){
		this.title = title;
		this.url = url;
		this.htmlContent = htmlContent;
	};
	
	@Override
	public String toString() {
		return "Page [title=" + title + ", url=" + url + ", htmlContent=" + htmlContent + "]";
	}

	public String htmlBuilder () {
		return htmlContent;
	} 
	
	public String loadPage () {
		return  title + ", " + url +", " + htmlContent;
	
	}

	public String refreshPage () {
		return  "refresh" + htmlContent;
	}
	
 	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHtmlContent() {
		return htmlContent;
	}

	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
	
	
}
