package homework2;

public class ForumPage extends Page implements CanOpenInChrome{

	public ForumPage(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
		
	}
	
	@Override
	public String htmlBuilder() {
		return this.getHtmlContent() + " is forum page";
	}


	@Override
	public String getHTMLInChrome(String htmlContent) {
		return htmlContent + " in Chrome";
	}
	
}
