package homework2;

public class HomePage extends Page implements CanOpenInChrome, CanOpenInFirefox{

	public HomePage(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	}
	
	@Override
	public String htmlBuilder() {
		return this.getHtmlContent() + " is home page";
	}


	@Override
	public String getHTMLInFirefox(String htmlContent) {
		return null;
	}

	@Override
	public String getHTMLInChrome(String htmlContent) {
		return null;
	}
	
}
