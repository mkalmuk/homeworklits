package homework2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WebSite {

	public static void main(String[] args) {
		Page aboutPage1 = new AboutPage("AboutPage", "about1.html", "About Page 111");
		Page aboutPage2 = new AboutPage("AboutPage", "about2.html", "About Page 222");
		Page forumPage1 = new ForumPage("ForumPage", "forum1.html", "Forum Page 111");
		Page forumPage2 = new ForumPage("ForumPage", "forum2.html", "Forum Page 222");
		Page loginPage  = new LoginPage("LoginPage", "login.html", "Login Page");
		Page homePage   = new HomePage("HomePage", "home.html", "Home Page");
		
		// 1 - all pages collection 
		List <Page> webSitePages = new ArrayList<Page>();
		webSitePages.add(aboutPage1);
		webSitePages.add(aboutPage2);
		webSitePages.add(forumPage1);
		webSitePages.add(forumPage2);
		webSitePages.add(loginPage);
		webSitePages.add(homePage);
		
		// 2 - all pages info
		System.out.println("All pages info:");
		for (Page page : webSitePages) {
			System.out.println(page.loadPage());
		}
		System.out.println("===========");
		
		// 3 - pages in Chrome
		System.out.println("Pages that opens only in Chrome:");
		for (Page page : webSitePages) {
			if (page instanceof CanOpenInChrome) {
				System.out.print(page.getUrl() + ", ");
			}
		}
		System.out.println("");
		
		// 3 - pages in FF 
		System.out.println("Pages that opens only in Firefox:");
		for (Page page : webSitePages) {
			if (page instanceof CanOpenInFirefox) {
				System.out.print(page.getUrl() + ", ");
			}
		}
		System.out.println("");
		
		System.out.println("===========");
		
		// 4 - htlmBuilder for each page
		System.out.println("htlmBuilder for each page:");
		for (Page page : webSitePages) {
			System.out.println(page.htmlBuilder());
		}
		
		System.out.println("===========");
		
		// 5 - unique titles 
		Set <String> uniqueTitles = new HashSet<String>();
		for (Page page : webSitePages) {
			uniqueTitles.add(page.getTitle());
		}
						
		System.out.print("unique titles: [ ");
		for (String title : uniqueTitles) {
			System.out.print(title + ", ");
		}
		System.out.println("]");
		
		System.out.println("===========");
		
		// 6 - map key = title, value - array of pages with that title
		Map <String, List<Page>> pagesMap = new HashMap<String, List<Page>>();
		for (String title : uniqueTitles) {
			List <Page> l1 = new LinkedList<Page>();
			for (Page page : webSitePages) {
				if (page.getTitle() == title) {
					l1.add(page);
				}
			}
			pagesMap.put(title, l1);
		}
		
		System.out.println("map of unique titles: [ ");
		for (Map.Entry<String, List<Page>> entry : pagesMap.entrySet()){
			System.out.println(entry.getKey() + ", " + entry.getValue()); 
		}
		System.out.println("]");
	}

}
