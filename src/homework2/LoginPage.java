package homework2;

public class LoginPage extends Page implements CanOpenInChrome, CanOpenInFirefox {

	public LoginPage(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
	
	}
	
	@Override
	public String htmlBuilder() {
		return this.getHtmlContent() + " is login page";
	}

	
	@Override
	public String getHTMLInFirefox(String htmlContent) {
		return htmlContent + " in Firefox";
	}

	@Override
	public String getHTMLInChrome(String htmlContent) {
		return htmlContent + " in Chrome";
	}

}
