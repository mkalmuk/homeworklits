package homework2;

public class AboutPage extends Page implements CanOpenInFirefox{

	AboutPage(String title, String url, String htmlContent) {
		super(title, url, htmlContent);
		
	}
	
	@Override
	public String htmlBuilder() {
		return this.getHtmlContent() + " is about page";
	}

	@Override
	public String getHTMLInFirefox(String htmlContent) {
		return htmlContent + " in Firefox";
	}

}
