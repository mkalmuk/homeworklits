package homework2;

public interface CanOpenInFirefox {
	
	public String getHTMLInFirefox (String htmlContent);

}
