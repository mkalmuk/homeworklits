package homework1_1;

/*1. Написати програму для роботи з списком. 
 * У першій половині списку замінити всі входження деякого елементу Е1 на  будь-який інший елемент Е2;
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class replaceElement {

	public static void main(String[] args) {
		List <String> list = new ArrayList<>();
		
		Collections.addAll(list, "Hello", "Hello", "Hello");	
		System.out.println(list);
		
		List <String> replacedList = replaceElementInFirstHalf(list, "Hello", "hello");
		System.out.println(replacedList);

	}
	
	public static List <String> replaceElementInFirstHalf(List <String> list, String src, String res) {
		
		int middle = (int) Math.round(((double) list.size() / 2));
		//System.out.println(middle);
		
		for (int i=0; i < middle; i++) {
			if (list.get(i) == src) {
				list.set(i, res);
			}
		}
		
		return list;
	}

}
