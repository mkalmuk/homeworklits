package homework3_2;

import java.time.LocalDate;

/*Написати метод, який приймає параметром поточну дату 
 * та додає дні, місяці та рік до цієї дати. 
 * Усі ці значення теж передаються параметрами в метод .
 */

public class shiftDate {

	public static LocalDate shiftDateBy (LocalDate date, int daysQuantity, int monthsQuantity, int yearsQuantity) {
		// days shift
		date = date.plusDays(daysQuantity);
		
		// months shift
		date = date.plusMonths(monthsQuantity);
		
		// years shift
		date = date.plusYears(yearsQuantity);
		
		return date;
	}
	
	public static void main(String[] args) {
		// today 
		LocalDate day = LocalDate.now();
		
		System.out.println("today is " + day);
		System.out.println("today + 2 days: " + shiftDateBy(day, 2, 0, 0));
		System.out.println("today - 5 months: " + shiftDateBy(day, 0, -5, 0));
		System.out.println("today + 3 years: " + shiftDateBy(day, 0, 0, 3));
		System.out.println("today - 4 years, 1 month, 28 days: " + shiftDateBy(day, -28, -1, -4));
		
	}

}
