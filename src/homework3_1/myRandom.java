package homework3_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class myRandom {
	
	/*1. Написати метод, який генерує випадкову колекцію стрічок, 
	 * кожна з яких є стрічкою, що містить від 6 до 15 символів. 
	 * Кількість стрічок в колекції передається, як параметр методу.. */

	private static final String ALPHA_NUM_SPL_CHARS  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*_=+-/";


	public static String randomString(int length) {
		if (length <= 0 )
			throw new IllegalArgumentException("Min. Length > Max. Length!");
		
		StringBuffer random = new StringBuffer();
		int index = 0;
	    for (int i = 0; i < length; i++) {
	      index = randomInt(0, ALPHA_NUM_SPL_CHARS.length());
	      random = random.append(ALPHA_NUM_SPL_CHARS.charAt(index));
	    }

		return random.toString();
	}
	
	public static int randomInt(int minLength, int maxLength) {
		Random rnd = new Random();
		int randomNumber = minLength + rnd.nextInt(maxLength - minLength);
		
		return randomNumber;
	}

	public static void main(String[] args) {
		List <String> stringCollection  = new ArrayList <String> ();
		int collectionSize  = randomInt(1, 100);
		
		for (int i = 0; i < collectionSize; i++) {
			stringCollection.add(randomString(randomInt(6, 15)));
		}
		
		System.out.println("Collection size: " + collectionSize);
		for (int i = 0; i < collectionSize; i++) {
			System.out.println(stringCollection.get(i));
		}
	
	}
}
























