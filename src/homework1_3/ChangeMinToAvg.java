package homework1_3;

import java.util.Arrays;
import java.util.Collections;

/* Найменший елемент масиву замінити цілою частиною середнього арифметичного всіх елементів. 
 * Якщо в масиві є декілька найменших елементів, то замінити останній з них.
 */

public class ChangeMinToAvg {

	public static void main(String[] args) {
		Integer [] numbers = {10, 10, 123, 1231, 3 , 123, 11, 10, 3, 1231};
		printArray(numbers);
		
		int avg = avgArray(numbers);
		System.out.println("Average: "+ avg);
		
		int minimum = Collections.min(Arrays.asList(numbers));
		Integer lastMinimumIndex = (Arrays.asList(numbers)).lastIndexOf(minimum);
		numbers[lastMinimumIndex] = avg;
		printArray(numbers);

	}
	
	public static void printArray(Integer [] numbers) {
		Integer size = numbers.length;
		
		System.out.print("[");
		for (int i = 0; i < size; i++) {
			System.out.print(numbers[i]);
			
			if (i != (size-1)) {
				System.out.print("; ");
			} 		
		}
		System.out.println("]");
	}
	
	public static Integer avgArray (Integer [] numbers) {
		Integer avg = 0;
		Integer size = numbers.length;
		
		for (int i = 0; i < size; i++) {
			avg = avg + numbers[i];
		}
		
		avg = (int) avg / size;
				
		return avg;
	}
	
}
