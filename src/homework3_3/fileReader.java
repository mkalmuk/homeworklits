package homework3_3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class fileReader { 
	 /* Написати метод, який зчитує дані з файлу у HashMap, 
	  * ключем якої є номер рядку файлу, а value - це текст у рядку файлу.
	  * Записати у інший файл тільки ті значення HashMap, ключі яких є степенем 2. 
	  * Використати перехоплення виключення ситуацій - (файлу не існує, файл порожній)
	  */
	
	public static HashMap<Integer, Double> powersMap(int base, int power){
		Map<Integer, Double> powersMap = new HashMap <Integer, Double>();
		
		for (int i = 0; i< power; i++) {
			powersMap.put(i, Math.pow(base, i));
		}
		
		return (HashMap<Integer, Double>) powersMap;
	}
	
	public static HashMap<Integer, String> readMap(String fileName) {
		File file = new File(fileName);
		//System.out.println(file.getAbsolutePath());
		Map<Integer, String> names = new HashMap<Integer, String>();
 	    
	    try {
	    	Scanner in = new Scanner(file);
		    int index  = 0;
		    
	    	while (in.hasNextLine()){
	    		names.put(index + 1, in.nextLine());
	    		index++;
	    	}
	    	in.close();
	    }
	    catch (FileNotFoundException e) {
	    	 System.out.println ("File not found");
	    }
	    catch (NullPointerException e) {
	    	System.out.println ("No path is specified");
	    }

	    return (HashMap<Integer, String>) names;
	}
	
	public static void writeMapToFile(String fileName, HashMap<Integer, String> mapToWrite) throws IOException{
		File file = new File(fileName);
		//System.out.println(file.getAbsolutePath()); 

		FileWriter fw = new FileWriter(file);
	    BufferedWriter bw = new BufferedWriter(fw);
	    
	    Map<Integer, Double> powersOfTwo = new HashMap<Integer, Double>();
	    //System.out.println(mapToWrite.size());
	    int resMapSize = log2 (mapToWrite.size());
	    //System.out.println("log 2 n = " + Integer.toString(resMapSize));
	    
		powersOfTwo = powersMap(2, resMapSize);
	    try {
	    	for (Map.Entry<Integer, String> entry : mapToWrite.entrySet()) {
	    		Integer key = entry.getKey();
	    	    String value = entry.getValue();
	  	    
	    	    if (powersOfTwo.containsValue((double)key)) {
	    	    	bw.write(key +", "+value + "\n");
	    	    }
			}
	    }
	    catch (IOException e) {
	    	System.out.println ("File exists but is a directory rather than a regular file, does not exist but cannot be created, or cannot be opened for any other reason");
	    }
	    finally {
	    	bw.close(); 
	    }
 	}
	
	public static int log2 (int number) {
		/** log with base 2
		 *  log 2 n = log e n / log e 2
		 */
		return (int) (Math.log(number) / Math.log(2)); 
	}; 
	
	public static void main (String args []) throws IOException {
		HashMap<Integer, String> stringsMap = readMap("src\\homework3_3\\MapSource.txt");
		
    	writeMapToFile("src\\homework3_3\\MapResult.txt", stringsMap);
	}
}
