package homework1_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/*2. Задано масив цілих чисел. Вивести масив в оберненому порядку, 
 * а потім видалити з нього повторні входження кожного елемента. 
*/

public class ReversedArrayNoDuplicates {

	public static void main(String[] args) {
		//initial array
		//Integer [] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		//Integer [] numbers = {1,2,3,4,5,1,2,3,4,5,6};
		Integer [] numbers = {6, 5, 4, 3, 2, 1, 5, 4, 3, 2, 1};
		
		// print initial array
		List<Integer> intList = Arrays.asList(numbers); 
		printArray(intList);
		
		//reversed array
		Collections.reverse(intList);
		printArray(intList);
		
		//reversed array without duplicates	
		List<Integer> intListNoDuplicates = new ArrayList<Integer>(new HashSet <Integer> (intList));
		printArray(intListNoDuplicates);
		
	}
	
	public static void printArray(List <Integer> numbers) {
		int size = numbers.size();
		
		System.out.print("[");
		for (int i = 0; i < size; i++) {
			System.out.print(numbers.get(i));
			
			if (i != (size-1)) {
				System.out.print("; ");
			} 		
		}
		System.out.println("]");
	}
	
	

}
