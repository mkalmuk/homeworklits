package book;

import java.util.Arrays;

public class EBook extends Book implements CanZoom{
	
	private String fileFormat;
	private int fileSize;
	private int price = 0;
	private int pagesInTrialVersion = 0;
	private int percentRead = 0;
	
	public EBook(String title, String[] authors, int publishYear) {
		super(title, authors, publishYear);	
	}
	
	public EBook(String title, String[] authors, int publishYear, String fileFormat, int fileSize, int percentRead) {
		
		this(title, authors, publishYear);
		this.setFileFormat(fileFormat);
		this.setFileSize(fileSize);
		this.setPercentRead(percentRead);
	}
	
	public EBook(String title, String[] authors, int publishYear,  
				String fileFormat, int fileSize, int percentRead, 
				int price, int pagesInTrialVersion) {
		
		this (title, authors, publishYear, fileFormat, fileSize, percentRead);
		this.setPrice(price);
		this.setPagesInTrialVersion(pagesInTrialVersion);
	}
		
	@Override
	public String toString() {
		return "eBook [title=" + this.getTitle() + ", authors=" + Arrays.toString(this.getAuthors()) 
		+ ", PublishYear=" + this.getPublishYear() + ", file format= " + fileFormat 
		+ ", file size = "+ fileSize +", price ="+ price + ", pagesInTrialVersion = " + pagesInTrialVersion 
		+ ", percent read = " + percentRead + "]";
	}
	
	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getPagesInTrialVersion() {
		return pagesInTrialVersion;
	}

	public void setPagesInTrialVersion(int pagesInTrialVersion) {
		this.pagesInTrialVersion = pagesInTrialVersion;
	}
	
	public int getPercentRead() {
		return percentRead;
	}

	public void setPercentRead(int percentRead) {
		this.percentRead = percentRead;
	}

	@Override
	public void openBook() {
		
	}

	@Override
	public void pageZoomIn(int curentPageNumber) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pageZoomOut(int curentPageNumber) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPageContent(int curentPageNumber) {
		// TODO Auto-generated method stub
		return null;
	};
	
}
