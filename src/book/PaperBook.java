package book;

import java.util.Arrays;

public class PaperBook extends Book{

	String ISBN;
	int price;
	int pageQuantity;
	
	public PaperBook(String title, String[] authors, int publishYear, int pageQuantity) {
		super(title, authors, publishYear);
	}
	

	@Override
	public String toString() {
		return "PaperBook [title=" + this.getTitle() + ", authors =" + Arrays.toString(this.getAuthors())
				+ ", PublishYear=" + this.getPublishYear() +", ISBN=" + ISBN + ", price=" + price 
				+ ", pageQuantity=" + pageQuantity + "]";
	}


	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getPageQuantity() {
		return pageQuantity;
	}

	public void setPageQuantity(int pageQuantity) {
		this.pageQuantity = pageQuantity;
	}
	
	
	
}
