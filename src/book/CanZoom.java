package book;

public interface CanZoom {
   public void pageZoomIn(int curentPageNumber);
   public void pageZoomOut(int curentPageNumber);
   public String getPageContent(int curentPageNumber);
  
}
