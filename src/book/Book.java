package book;

import java.util.Arrays;
import java.util.Random;
import java.time.LocalDate;
//import java.time.LocalDateTime;

abstract class Book {
	
	private String title;
	private String [] authors;
	private int PublishYear;
	private int pageQuantity;
	private LocalDate lastOpened;
			
	public Book(String title, String[] authors, int publishYear) {
		super();
		this.title = title;
		this.authors = authors;
		PublishYear = publishYear;
	}
	
	@Override
	public String toString() {
		return "Book [title=" + title + ", authors=" + Arrays.toString(authors) 
		+ ", PublishYear=" + PublishYear + "]";
	}
		
	public String getTitle() { 
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String[] getAuthors() {
		return authors;
	}
	
	public void setAuthors(String[] authors) {
		this.authors = authors;
	}
	
	public int getPublishYear() {
		return PublishYear;
	}
	
	public void setpageQuantity(int pageQuantity) {
		this.pageQuantity = pageQuantity;
	}

	public LocalDate getLastOpened() {
		return lastOpened;
	}

	public void setLastOpened(LocalDate lastOpened) {
		this.lastOpened = lastOpened;
	}

	public int nextPage(int currentPage) {
		return currentPage++;
	}
	
	public int randomPage() {
		//new Random().nextInt((max - min) + 1) + min 
		// where max= pageQuantity-1, min = 1 
		return 	new Random().nextInt(pageQuantity-1) + 1;
	}
	
	public void openBook() {
		
	};
}
		


