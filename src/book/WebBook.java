package book;

import java.util.Arrays;

public class WebBook extends Book implements CanZoom{

	public WebBook(String title, String[] authors, int publishYear) {
		super(title, authors, publishYear);
		
	}
	
	@Override
	public String toString() {
		return "Web book [title=" + this.getTitle() + ", authors=" + Arrays.toString(this.getAuthors()) 
		+ ", PublishYear=" + this.getPublishYear() + "]";
	}
	
	

	@Override
	public void pageZoomIn(int curentPageNumber) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pageZoomOut(int curentPageNumber) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPageContent(int curentPageNumber) {
		// TODO Auto-generated method stub
		return null;
	}

}
